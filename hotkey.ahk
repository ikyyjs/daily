#NoEnv
#SingleInstance, Force

CheckAutoStartup()

CheckAutoStartup()
{
    ; Membaca nilai kunci registri untuk mengecek apakah skrip AutoHotKey sudah ada di auto startup
    RegRead, AutoHotkeyScript, HKEY_CURRENT_USER, Software\Microsoft\Windows\CurrentVersion\Run, AutoHotkeyScript
    
    ; Jika nilai kunci registri tidak sama dengan jalur lengkap skrip ini, maka update nilai registri
    if (AutoHotkeyScript != A_ScriptFullPath)
        RegWrite, REG_SZ, HKEY_CURRENT_USER, Software\Microsoft\Windows\CurrentVersion\Run, AutoHotkeyScript, %A_ScriptFullPath%
}

CreateTempScript(command) 
{
  ; Mendefinisikan konten skrip
  scriptContent := command

  ; Membuat file skrip sementara
  scriptFile := A_Temp . "\temp.bat"
  FileDelete, %scriptFile%
  FileAppend, %scriptContent%, %scriptFile%

  ; Menjalankan skrip menggunakan cmd
  Run, %comspec% /c %scriptFile%,, hide

  ; Opsional: Menunggu skrip selesai sebelum menghapus file
  WinWaitClose, ahk_exe cmd.exe

  ; Menghapus file skrip sementara
  FileDelete, %scriptFile%
}





;========================================================
;			Enviroment Variables
;========================================================

^!+p::
  ; Membuka jendela untuk mengedit variabel lingkungan sistem
  CreateTempScript("rundll32 sysdm.cpl,EditEnvironmentVariables")
return





;========================================================
;		    Folder Shortcuts
;========================================================

; ^+!f::
;   ; Membuka File Explorer di direktori proyek Flutter
;   CreateTempScript("cd %USERPROFILE%\Documents\FLUTTER_PROJECT && explorer .")
; return

^+!w::
  ; Membuka File Explorer di direktori Laragon
  CreateTempScript("cd C:\laragon\www && explorer .")
return





;========================================================
;			Web Links
;========================================================

OpenWebsite(url)
{
    ; Membuka situs web menggunakan browser default
    Run, %url%
}

^+!l::
  ; Membuka situs YouTube menggunakan browser default
  OpenWebsite("https://9code.id/")
return

^+!y::
  ; Membuka situs YouTube menggunakan browser default
  OpenWebsite("https://www.youtube.com")
return





;========================================================
;		    	Applications
;========================================================

OpenApplication(path)
{
    ; Membuka aplikasi menggunakan jalur yang diberikan
    Run, % """" path """"
}

^+!.::
  ; Membuka Microsoft Edge Dev menggunakan jalur yang diberikan
  OpenApplication("C:\Program Files (x86)\Microsoft\Edge Dev\Application\msedge.exe")
return


::dartmain::
(
void main() {
    // Baris 1
    print('Hello, Dart!');

    // Baris 2
    int angka = 42;

    // Baris 3
    String teks = 'Ini adalah contoh snippet Dart';

    // ... Lanjutkan dengan lebih banyak baris kode ...

    // Baris 100
    for (int i = 0; i < 10; i++) {
        print('Iterasi ke-${i + 1}');
    }

    // Baris 101
    print('Snippet Dart dengan lebih dari 100 baris!');
}
)
