rmdir /s /q .git
git init
git remote remove origin
git remote add origin %1
git branch -M main
git add .
git commit -m ":tada: initial: Initial commit"
git push --set-upstream origin main --force