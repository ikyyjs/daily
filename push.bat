@echo off
chcp 65001 > nul 

echo +---------------------- Contoh Template Commit Message -----------------------+
echo +----+----+-------------+--------------+--------------------------------------+
echo ^| NO ^| 😉 ^| TYPE        ^| SCOPE        ^| DESCRIPTION                          ^|
echo +----+----+-------------+--------------+--------------------------------------+
echo ^| 1. ^| ✨ ^| feat        ^| Homescreen   ^| Tambahkan tombol Tambah Kontak       ^|
echo ^| 2. ^| 🐛 ^| fix         ^| Login        ^| Perbaiki bug login                   ^|
echo ^| 3. ^| 📚 ^| docs        ^| Readme       ^| Update dokumentasi                   ^|
echo ^| 4. ^| 🌈 ^| style       ^| Header       ^| Sesuaikan tata letak dan gaya header ^|
echo ^| 5. ^| 📦 ^| refactor    ^| API          ^| Refaktor kode pengelolaan API        ^|
echo ^| 6. ^| 🚀 ^| perf        ^| Search       ^| Optimalkan algoritma pencarian       ^|
echo ^| 7. ^| 🚨 ^| test        ^| Unit         ^| Tambahkan pengujian unit login       ^|
echo ^| 8. ^| 🛠  ^| build       ^| Dependency   ^| Update versi dependensi utama        ^|
echo ^| 9. ^| ⚙️  ^| ci          ^| Build        ^| Integrasikan skrip pembersihan kode  ^|
echo ^| 10.^| 🚧 ^| chore       ^| Codebase     ^| Bersihkan dan susun struktur kode    ^|
echo ^| 11.^| 🗑  ^| revert      ^| Feature      ^| Kembalikan perubahan fitur           ^|
echo ^| 12.^| 🔴 ^| dependencies^| Firebase     ^| Update versi Firebase                ^|
echo ^| 13.^| ⚠️  ^| metadata    ^| App          ^| Sesuaikan metadata aplikasi          ^|
echo +----+----+-------------+--------------+--------------------------------------+
         

set /p choice="Nomor: "

if %choice% lss 1 goto invalidChoice
if %choice% gtr 13 goto invalidChoice

if "%choice%" equ "1" set emoji=":sparkles:"
if "%choice%" equ "2" set emoji=":bug:"
if "%choice%" equ "3" set emoji=":books:"
if "%choice%" equ "4" set emoji=":rainbow:"
if "%choice%" equ "5" set emoji=":package:"
if "%choice%" equ "6" set emoji=":rocket:"
if "%choice%" equ "7" set emoji=":rotating_light:"
if "%choice%" equ "8" set emoji=":hammer_and_pick:"
if "%choice%" equ "9" set emoji=":gear:"
if "%choice%" equ "10" set emoji=":construction:"
if "%choice%" equ "11" set emoji=":wastebasket:"
if "%choice%" equ "12" set emoji=":red_circle:"
if "%choice%" equ "13" set emoji=":warning:"

if "%choice%" equ "1" set type="feat"
if "%choice%" equ "2" set type="fix"
if "%choice%" equ "3" set type="docs"
if "%choice%" equ "4" set type="style"
if "%choice%" equ "5" set type="refactor"
if "%choice%" equ "6" set type="perf"
if "%choice%" equ "7" set type="test"
if "%choice%" equ "8" set type="build"
if "%choice%" equ "9" set type="ci"
if "%choice%" equ "10" set type="chore"
if "%choice%" equ "11" set type="revert"
if "%choice%" equ "12" set type="dependencies"
if "%choice%" equ "13" set type="metadata"

set /p scope="Scope: "
set /p description="Description: "

git add .
git commit -m "%emoji% %type%(%scope%): %description%"
git push

echo YEAY commitan kamu, berhasil!!🤩🥳

goto :eof

:invalidChoice
echo Pilihan tidak valid. Silakan pilih nomor yang sesuai dengan opsi yang tersedia.
goto :eof

